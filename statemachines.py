# Super class for a state object
class State:
    stateMachine = None

    def Execute(self):
        pass

    def Enter(self):
        pass

    def Exit(self):
        pass

# Implementation of a state machine for running states
class StateMachine:
    def __init__(self, state):
        self.state = state
        self.state.stateMachine = self

    def Run(self):
        self.state.Enter()
        self.running = True
        while self.running:
            self.state.Execute()

    def ChangeState(self, newState):
        self.state.Exit()
        self.state = newState
        self.state.stateMachine = self
        self.state.Enter()

# Supplementary statemachine for the purporses of this specific project
class mySt(StateMachine):
    def __init__(self, state):
        super().__init__(state)
        self.socket = None #General socket for com.
        self.conn = None #Specific socket connection
        self.camcolor = None #OAK color Camera
        self.camdepth = None #OAK depth Camera
        self.pl = None #Pipeline
