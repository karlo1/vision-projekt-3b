import IMPP
import numpy as np
from statemachines import *
from state import *

#This program can be used to determine optimal values for HSV threshholding and circle detection
#Diagnostic tools for depth detection is also included

print('¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨') #Extra space termnial
print('') #Extra space termnial

print("Setting up camera...")#<------------------------------------------------CAM
cam = OAKCamColorDepth()

print("Setting up pipeline...")#<----------------------------------------------PIPE
pl = IMPP.PostProcessingPipeline([
IMPP.GaussianBlur(5, trackbar = False, showOutput=False),
IMPP.ConvertRGB2HSV(showOutput=False),
IMPP.HSVThreshold(lowerBound = np.array([0,0,0]), upperBound = np.array([255,255,255]), trackbar = True, showOutput=False),
IMPP.ConvertToGray(showOutput=False),
IMPP.CircleDetection(Filter=2, min_dist=1000,minR=50, maxR=65, UTICED=200, ThrCD=200, trackbar = True, showOutput=False)])

while True:

    #Capture frames from camera
    depth_frame = cam.getDepthFrame()   #Depth frame,        1280 * 720
    frame = cam.getFrame()              #Normal color frame, 1920 * 1080

    #Run pipepline for frame manipulation
    coordinates, output = pl.run(frame)
    #coordinates = list of 2D coordinates for detected circle in pixel value
    #otput = image where detected circle can be seen

    #Define variables for later use
    y_flip = None
    x = None
    y = None

    #Run only if a circle is detected
    #Write detected coordinates for frame [x,y] and for depth_frame [skalering]
    if coordinates != 'NO CIRCLE DETECTED':
        x = coordinates[0]
        y = coordinates[1]
        y_flip = 1080 - y #y-axis is upside down when image is read, so this flips it rightside up

        scale_x = round(0.6 * coordinates[0]) #scale from frame coordinates to depth_depth fram coordinates
        y_flip_scale = round(0.6 * coordinates[1]) #scale from frame coordinates to depth_depth fram coordinates

        scale_y = 720 - y_flip_scale #y-axis is upside down when image is read, so this flips it rightside up

    #If no circle is detected, a constant coordinat value is used for reading of depth
    #This is so the depth reading can be tested on other things than detected circles
    else:
        scale_x = 360
        scale_y = 540

    #Ensures that the inspected pixel (and its neighbourhood) is always within frame resolution boundaries
    if scale_x >= depth_frame.shape[1]-1:
        scale_x = depth_frame.shape[1]-4

    if scale_y >= depth_frame.shape[0]-1:
        scale_y = depth_frame.shape[0]-4

    #Averaging inspected pixel value over it's approximate neighbourhood
    Rz_raw = depth_frame[scale_y,scale_x]
    Rz_raw += depth_frame[scale_y,scale_x+1]
    Rz_raw += depth_frame[scale_y,scale_x-1]
    Rz_raw += depth_frame[scale_y+1,scale_x]
    Rz_raw += depth_frame[scale_y-1,scale_x]
    Rz_raw += depth_frame[scale_y+1,scale_x+1]
    Rz_raw += depth_frame[scale_y+1,scale_x-1]
    Rz_raw += depth_frame[scale_y-1,scale_x+1]
    Rz_raw += depth_frame[scale_y-1,scale_x-1]

    Rz_raw += depth_frame[scale_y,scale_x+2]
    Rz_raw += depth_frame[scale_y,scale_x-2]
    Rz_raw += depth_frame[scale_y+2,scale_x]
    Rz_raw += depth_frame[scale_y-2,scale_x]
    Rz_raw += depth_frame[scale_y+2,scale_x+2]
    Rz_raw += depth_frame[scale_y+2,scale_x-2]
    Rz_raw += depth_frame[scale_y-2,scale_x+2]
    Rz_raw += depth_frame[scale_y-2,scale_x-2]

    ave = round(Rz_raw/17)

    print(f"Average value depth pixel intensity: {ave}")
        
    #Convert depth_frame to color so X is visible as green
    depth_clr = cv2.cvtColor(depth_frame, cv2.COLOR_GRAY2RGB)

    #Making an "X" to mark the pixel coordinat on depth_frame and frame
    cv2.putText(output, "X", (x, y_flip), cv2.FONT_HERSHEY_PLAIN, 5, (0, 255, 0), 5,
                        cv2.LINE_AA)
    cv2.putText(depth_clr, "X", (scale_x, scale_y), cv2.FONT_HERSHEY_PLAIN, 5, (0, 30000, 0), 5,
                        cv2.LINE_AA)


    #Resize so windows fit on laptop screen
    depth_clr = cv2.resize(depth_clr, (540, 360), interpolation=cv2.INTER_LINEAR)
    output = cv2.resize(output, (540, 360), interpolation=cv2.INTER_LINEAR)

    #Shows windows
    cv2.imshow('10', output)
    cv2.imshow('20', depth_clr)

    if cv2.waitKey(15) == ord('q'):
        break
