# Vision - Projekt 3B
This project focuses on creating a visiual solution to detect tomatoes, and navigate a robot to the detected tomato.

Another task for the group is to create a new end-tool for the robot, created specific for the task of grabbing tomatoes.

## Devices
Camera: OAK-D
Robot: Universal Robot UR5e
End-tool: Arduino Nano
Vision: Computer

## Dependencies
OpenCV

## Solution
The vision solution converts the rgb to hsv to do a precise color threshold of the image.
Using the depth, there is made a simple filter that looks after specific ranges.

Communication is done through TCP/IP, and it is run through a statemachine.

## Contributions
    IMPP.py - Pipeline - # By Mathias Gregersen, megr@ucl.dk

## Project status
Program is running, but this is only a Proof Of Concept and have only been used on a non-organic model to test the solution..