from statemachines import *
from state import *

#Start the statemachine controlling the camera/server part of the tomato-picking solution
#Define starting state
startingState = State00_INIT()

#Create statemachine starting from starting state
sm = mySt(startingState)

#Run statemachine from starting state
sm.Run()
#hello


