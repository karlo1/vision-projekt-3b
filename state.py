from statemachines import * 
import cv2
import numpy as np
#import IMPP
from Wrapper import *
#import socket
import setup_functions as sf
import time

class State00_INIT(State): #Setup of socket, camera and pipeline

    def Enter(self):
        print('¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨')
        print('')

        #socket setup
        self.stateMachine.socket = sf.setup_socket() 

        #camera setup
        print("...Setting up camera...")#<----------------------------------------------CAM
        self.stateMachine.cam = OAKCamColorDepth()

        #self.stateMachine.cam = cv2.VideoCapture(0) #webcam for testing without OAKcam
        print('')

        #pipeline setup
        self.stateMachine.pl = sf.setup_pipeline() #

        print("_>_>_> RUN PROGRAM <_<_<_")
        print('')
        print('¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨')
        print('')

        self.stateMachine.ChangeState(State10_Listen()) #switch to next state

class State10_Listen(State): #Activate socket listening

    def Enter(self):
        #Listen on socket
        self.stateMachine.socket.listen()
        print("STATE10_Listen:::::::> - Listening on socket...")
        print('')

        self.stateMachine.ChangeState(State20_Connect())#switch to next state

class State20_Connect(State): #Connect to client

    def Enter(self):
        #accept incoming connection and assign port number to communication channel
        self.stateMachine.conn, addr = self.stateMachine.socket.accept() #addr = the PORT number that the server har given to connection client upon connection
        print(f"-> Server connected by {addr}")
        print("''''''''''''''''''''''''''''''''''''''''''''''''''")
        print('')
        print('')

        self.stateMachine.ChangeState(State30_Wait())#switch to next state

class State30_Wait(State): #Waiting for robot to request coordinates

    def Enter(self):
        print("STATE30_Wait:::::::> - Standby: waiting on client...")
        print('')

        #Waiting for request from robot for coordinates
        data = self.stateMachine.conn.recv(4096)

        #Printout of robots exact request
            #print("Data received from client: " + data.decode()) #for testing and debugging

        #Return message to client for confirmation
            #self.conn.sendall(data) #for testing and debugging

        print("''''''''''''''''''''''''''''''''''''''''''''''''")
        print('')
        print('')

        self.stateMachine.ChangeState(State40_Find())#switch to next state
 
class State40_Find(State): #Find coordinates
    
    def Enter(self):                
        print("STATE40_Find:::::::> - Finding coordinates for tomato...")
        print('')  
        print("getting frame...")
        print("running pipeline...")

    def Execute(self):
        #getting frame from camera

        time.sleep(3)
        frame = self.stateMachine.cam.getFrame() #Normal color frame (1280x720)
        depth_frame = self.stateMachine.cam.getDepthFrame() #Frame indicating depth (1080x720)
        #ret, frame = self.stateMachine.cam.read() #for testing with webcam

        #Run pipeline for tomato detection
        #-Output is image with circles drawn
        #-coordinates is the coordinates for the detected circle as a list with two values       
        coordinates, output = self.stateMachine.pl.run(frame)

        #Resize output from OAK resolution to smaller resolution so it fits on laptop screen
        #OAK resolution: 12MP (4032x3040)
        output = cv2.resize(output, (1080, 720), interpolation=cv2.INTER_LINEAR)

        #If no circle is detected, go back to beginning of 'Execute'
        if coordinates != 'NO CIRCLE DETECTED':
            #If circle is detected, run this:
            print('Cam coordinates: ' + str(coordinates)) #print detected coordinates
            print('')


            #NOTICE: tool position x, y and z is in meters, but is displayed as milimeters in PolyScope
            #And feature is "base" (not custom plane?) not "view" or "tool" this has impact on z value            
            
            #temporary test of coordinates that can be send to client.
            #test start     
                   
            y = coordinates[1]

           # print(depth_frame[scaling_y, scaling_x])
            if y >= 720:
                print('coordinate outside array')
                y = 719
          

            Rx = (((0.5775027 * coordinates[0]) -(359.4878))/1000)
            Ry = (((0.60423 * coordinates[1]) -(175.8308) )/1000)

            #Coordinates where Z is constant
            iCoordinates = [Rx,Ry,-0.010,0.651,3.029,0.023]         
            
            #Coordinates where z is variable
            # scaling_x : int = round((0.66666 * coordinates[0]))
            # Rz_raw = depth_frame[y, scaling_x]
            # Rz = (-1.05 * Rz_raw) + 119.8
            # calculated_Z = (Rz - 239)/1000            
            # iCoordinates = [Rx,Ry,calculated_Z,0.651,3.029,0.023]
           

            print('UR koord: ' + str(iCoordinates))
            print("'''''''''''''''''''''''''''''''''''''''")
            print('')
            print('')

            cv2.imshow('', output) 

            cv2.waitKey(2000)
            cv2.destroyAllWindows()
            cv2.waitKey(15)
            self.stateMachine.ChangeState(State50_Send(iCoordinates))#switch to next state - iCoordinates = coordinates to be send to next state


        cv2.imshow('', output)      

        # For testing
        cv2.waitKey(15)
            
class State50_Send(State): #Send coordinates to robot


    def __init__(self, iCoordinates) -> None:     
        super().__init__()
        self.coordinates = str(iCoordinates) #iCoordinates = found coordinates from previous state

    def Enter(self):

        
        #Send coordinates to robot
        print("STATE50_Send:::::::> - Sending coordinates to robot")
        print('')
        self.stateMachine.conn.sendall(self.coordinates.encode())

        #Wait for confirmation of coordinates received by robot
        print('Waiting for confirmation...')
        confirmation = self.stateMachine.conn.recv(4096)
        print('Message from client: ' + str(confirmation.decode()))
        print('')
        print('')

        self.stateMachine.ChangeState(State30_Wait())#switch to next state
