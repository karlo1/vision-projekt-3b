import socket
import IMPP
import numpy as np

def setup_socket():
    print("...Setting up socket...")#<----------------------------------------------SOCKET
    #HOST = "127.0.0.1"  # Standard loopback interface address (localhost) - for testing
    HOST = "192.168.1.42" # PC's IP-address
    PORT = 65432  # Port to listen on (non-privileged ports are > 1023)

    #make socket objekt
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #bind socket objekt to adresses
    s.bind((HOST, PORT))
    print("<<<Socket setup complete>>>")
    print('')

    return s

def setup_pipeline():
    print("...Setting up pipeline...")#<----------------------------------------------PIPE

    #Define pipeline: include functions and their parameters
    pl = IMPP.PostProcessingPipeline([
    IMPP.GaussianBlur(3, trackbar = False, showOutput=False),
    IMPP.ConvertRGB2HSV(showOutput=False),
    IMPP.HSVThreshold(lowerBound = np.array([120,255,30]), upperBound = np.array([120,255,120]), trackbar = False, showOutput=False),
    IMPP.ConvertToGray(showOutput=False),
    IMPP.CircleDetection(Filter=2, min_dist=1000,minR=48, maxR=65, UTICED=40, ThrCD=38, trackbar = False, showOutput=False)])
    print("<<<Pipeline setup complete>>>")
    print('')

    return pl
