
#coordinate math

#Robot Z=0 for each point used on workplane.

#---------
#|1       |
#|        |
#|2      3|
#---------


#Above to the left (1)
Rx1 = 0
Px1 = 623


Ry1 = 200
Py1 = 622


#Below to the left (2)
Rx2 = 0
Px2 = 623

Ry2 = 0
Py2 = 291


#Below to the right (3)
Rx3 = 427
Px3 = 1363

Ry3 = 0
Py3 = 291


#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# https://www.wolframalpha.com/widgets/view.jsp?id=e6bfd79be503e98ee35900cc07b0d5eb
# Rx(Px,Py) = a*Px + b*Py + c

#   0=(a*623)+(b*781)+c
#   0=(a*623)+(b*291)+c
#   427=(a*1363)+(b*291)+c


Pxa = 57/148
Pxb = 0
Pxc = -(35511/148)

#YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
# https://www.wolframalpha.com/widgets/view.jsp?id=e6bfd79be503e98ee35900cc07b0d5eb
# Ry(Px,Py) = a*Px + b*Py + c

#   280=(a*623)+(b*291)+c
#   0=(a*623)+(b*285)+c
#   0=(a*1310)+(b*285)+c


Pya = 0
Pyb = 4/7
Pyc = -(1164/7)


print("______________________________")
print('')
print('')
print(f"a = {Pxa}, b = {Pxb}, c = {Pxc} ")
print('')
print('')
print(f"a = {Pya}, b = {Pyb}, c = {Pyc} ")
print('')
print('')

coordinate = [0,285]

Rx = ((1.77458 * coordinate[0]) + 570)/1000
Ry = ((1.73684 * coordinate[1]) + 285)/1000

print("Rx= " + str(Rx))
print("Ry= " + str(Ry))

# NEW
 # x axis calculation
# (a*568)+(b*310)+c=0
# (a*1323)+(b*310)+c=431
# (a*568)+(b*985)+c=0

# a = 0,5708 b = 0 c = -324,249

 # y axis calculation
# (a*568)+(b*310)+c=0
# (a*1323)+(b*310)+c=0
# (a*568)+(b*985)+c=359

# a = 0 b = 0,5318 c = -164,874
